package com.graphQLSpring.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.graphQLSpring.Model.Post;

public interface PostRepository extends PagingAndSortingRepository<Post, Long> {
	
	@Query(value="SELECT p FROM Post p JOIN FETCH p.author a",
		   countQuery="SELECT COUNT(p) FROM Post p")
	public Page<Post> findAll(Pageable pageable);
	
}
