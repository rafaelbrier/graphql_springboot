package com.graphQLSpring.Repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.graphQLSpring.Model.Author;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {

}
