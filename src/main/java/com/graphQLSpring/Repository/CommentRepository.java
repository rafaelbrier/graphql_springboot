package com.graphQLSpring.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.graphQLSpring.Model.Comment;

public interface CommentRepository extends PagingAndSortingRepository<Comment, Long> { 
	
	@Query(value="SELECT * FROM Comment AS c WHERE c.Post_Id = :postId", nativeQuery=true)
	public Page<Comment> findAllCommentsByPostId(
			@Param("postId") Long postId,
			Pageable pageable
			);
}
