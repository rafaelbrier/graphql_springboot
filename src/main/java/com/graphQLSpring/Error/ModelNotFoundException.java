package com.graphQLSpring.Error;

import java.util.Map;

public class ModelNotFoundException extends GraphQLErrorModel {

	private static final long serialVersionUID = 1L;

	public ModelNotFoundException(String message, Map<String, Object> extensions) {
		super(message, extensions);
	}
} 
