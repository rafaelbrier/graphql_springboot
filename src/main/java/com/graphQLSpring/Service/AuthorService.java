package com.graphQLSpring.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.graphQLSpring.Model.Author;
import com.graphQLSpring.Repository.AuthorRepository;

@Service
public class AuthorService {
	
	@Autowired
	private AuthorRepository authorRepository;

	public Page<Author> findAll(Pageable pageable) {
		return this.authorRepository.findAll(pageable);
	}
	
	public Author findById(Long id) {
		return this.authorRepository.findById(id).orElse(null);
	}
	
	public Author save(Author author) {
		return this.authorRepository.save(author);
	}
	
	public Long count() {
		return this.authorRepository.count();
	}
}
