package com.graphQLSpring.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.graphQLSpring.Model.Post;
import com.graphQLSpring.Repository.PostRepository;

@Service
public class PostService {
	
	@Autowired
	private PostRepository postRepository;

	public Page<Post> findAll(Pageable pageable) {
		return this.postRepository.findAll(pageable);
	}
	
	public Post findById(Long id) {
		return this.postRepository.findById(id).orElse(null);
	}
	
	public Post save(Post post) {
		return this.postRepository.save(post);
	}
	
	public Long count() {
		return this.postRepository.count();
	}
}
