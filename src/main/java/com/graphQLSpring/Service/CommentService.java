package com.graphQLSpring.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.graphQLSpring.Model.Comment;
import com.graphQLSpring.Repository.CommentRepository;

@Service
public class CommentService {
	
	@Autowired
	private CommentRepository commentRepository;
	
	public Page<Comment> findAll(Pageable pageable) {
		return this.commentRepository.findAll(pageable);
	}
	
	public Page<Comment> findAllCommentsOfPost(Long postId, Pageable pageable) {
		return this.commentRepository.findAllCommentsByPostId(postId, pageable);
	}
	
	public Comment findById(Long id) {
		return this.commentRepository.findById(id).orElse(null);
	}
	
	public Comment save(Comment comment) {
		return this.commentRepository.save(comment);
	}
	
	public Long count() {
		return this.commentRepository.count();
	}
}
