package com.graphQLSpring;

import java.time.LocalDateTime;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.graphQLSpring.Model.Author;
import com.graphQLSpring.Model.Comment;
import com.graphQLSpring.Model.Post;
import com.graphQLSpring.Service.AuthorService;
import com.graphQLSpring.Service.CommentService;
import com.graphQLSpring.Service.PostService;

@Configuration
public class InitializeData {

	@Bean
	public CommandLineRunner loadInitialData(
			AuthorService authorService,
			CommentService commentService,
			PostService postService) {
		return (args) -> {

			LocalDateTime rightNow = LocalDateTime.now();

			for (long i = 1; i <= 10; i++) {
				Author author = new Author("Author_"+i, "I'm the Author " + i);
				authorService.save(author);
				Post post = new Post("Titulo_"+i, "Corpo da Notícia "+i, author, rightNow.toString());
				postService.save(post);
				Comment comment = new Comment("Comment_"+i, author, post, rightNow.toString());
				commentService.save(comment);
			}
		};
	}

}
