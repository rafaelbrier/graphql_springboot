package com.graphQLSpring.Utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public final class PaginationUtils {
	
	private final static Integer maxPageSize = 150;
	
	public static Pageable buildPageable(Integer pageNumber, Integer pageSize) {
		if(pageNumber == null) {
			pageNumber = 0;
		}
		if(pageSize == null) {
			pageSize = maxPageSize;
		}
		return PageRequest.of(pageNumber, pageSize);
	}
}
