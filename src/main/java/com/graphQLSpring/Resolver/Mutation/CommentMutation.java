package com.graphQLSpring.Resolver.Mutation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.google.common.collect.ImmutableMap;
import com.graphQLSpring.Error.ModelNotFoundException;
import com.graphQLSpring.Model.Comment;
import com.graphQLSpring.Service.CommentService;

@Component
public class CommentMutation implements GraphQLMutationResolver {

	@Autowired
	private CommentService commentService;

	public Comment newComment(Comment comment) {
		commentService.save(comment);
		return comment;
	}

	public Comment updateComment(Long id, Comment comment) {
		Comment commentToUpdate = this.commentService.findById(id);
		if(commentToUpdate == null) {
			throw new ModelNotFoundException("The Comment to be updated does not exist!",
					ImmutableMap.of("invalidId", id));
		} 
		comment.setId(id);
		commentService.save(comment);
		return comment;
	}
}
