package com.graphQLSpring.Resolver.Mutation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.google.common.collect.ImmutableMap;
import com.graphQLSpring.Error.ModelNotFoundException;
import com.graphQLSpring.Model.Post;
import com.graphQLSpring.Service.PostService;

@Component
public class PostMutation implements GraphQLMutationResolver {
	@Autowired
	private PostService postService;

	public Post newPost(Post post) {
		postService.save(post);
		return post;
	}
	
	public Post updatePost(Long id, Post post) {
		Post postToUpdate = this.postService.findById(id);
		if(postToUpdate == null) {
			throw new ModelNotFoundException("The Post to be updated does not exist!",
					ImmutableMap.of("invalidId", id));
		} 
		post.setId(id);
		postService.save(post);
		return post;
	}
}