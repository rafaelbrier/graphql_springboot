package com.graphQLSpring.Resolver.Mutation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.google.common.collect.ImmutableMap;
import com.graphQLSpring.Error.ModelNotFoundException;
import com.graphQLSpring.Model.Author;
import com.graphQLSpring.Service.AuthorService;

@Component
public class AuthorMutation implements GraphQLMutationResolver {

	@Autowired
	private AuthorService authorService;

	public Author newAuthor(Author author) {
		authorService.save(author);
		return author;
	}

	public Author updateAuthor(Long id, Author author) {
		Author authorToUpdate = this.authorService.findById(id);
		if(authorToUpdate == null) {
			throw new ModelNotFoundException("The Author to be updated does not exist!",
					ImmutableMap.of("invalidId", id));
		} 
		author.setId(id);
		authorService.save(author);
		return author;
	}
}