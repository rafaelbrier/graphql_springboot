package com.graphQLSpring.Resolver.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.graphQLSpring.Model.Author;
import com.graphQLSpring.Service.AuthorService;
import com.graphQLSpring.Utils.PaginationUtils;

@Component
public class AuthorQuery implements GraphQLQueryResolver {

	@Autowired
	private AuthorService authorService;

	public Page<Author> allAuthors(Integer pageNumber, Integer pageSize) {
		return authorService.findAll(PaginationUtils.buildPageable(pageNumber, pageSize));
	}

	public Long countAuthors() {
		return authorService.count();
	}
}