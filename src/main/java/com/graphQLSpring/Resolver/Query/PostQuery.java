package com.graphQLSpring.Resolver.Query;

import java.util.ArrayList;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.google.common.collect.ImmutableMap;
import com.graphQLSpring.Error.ModelNotFoundException;
import com.graphQLSpring.Model.Comment;
import com.graphQLSpring.Model.Post;
import com.graphQLSpring.Service.CommentService;
import com.graphQLSpring.Service.PostService;
import com.graphQLSpring.Utils.PaginationUtils;

@Component
public class PostQuery implements GraphQLQueryResolver {

	@Autowired
	private PostService postService;
	@Autowired
	private CommentService commentService;

	public Page<Post> allPosts(Integer pageNumber, Integer pageSize) {
		return this.postService.findAll(PaginationUtils.buildPageable(pageNumber, pageSize));
	}
	
	public Post Post(Long id, Boolean withComments) {
		Post post = this.postService.findById(id);
		if(post == null) {
			throw new ModelNotFoundException("The Post requested does not exist!",
					ImmutableMap.of("invalidId", id));
		} 
		if(BooleanUtils.isTrue(withComments) ) {
			Page<Comment> commentPage = this.commentService.findAllCommentsOfPost(id, PaginationUtils.buildPageable(null, null));
			post.setComments(commentPage.getContent());
		} else {
			post.setComments(new ArrayList<Comment>());
		}
		return post;
	}

	public Long countPosts() {
		return postService.count();
	}
}
