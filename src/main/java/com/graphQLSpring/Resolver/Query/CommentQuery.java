package com.graphQLSpring.Resolver.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.graphQLSpring.Model.Comment;
import com.graphQLSpring.Service.CommentService;
import com.graphQLSpring.Utils.PaginationUtils;

@Component
public class CommentQuery implements GraphQLQueryResolver {

	@Autowired
	private CommentService commentService;

	public Page<Comment> allComments(Integer pageNumber, Integer pageSize) {
		return commentService.findAll(PaginationUtils.buildPageable(pageNumber, pageSize));
	}
	
	public Page<Comment> allCommentsOfPost(Long postId, Integer pageNumber, Integer pageSize) {
		return this.commentService.findAllCommentsOfPost(postId, PaginationUtils.buildPageable(pageNumber, pageSize));
	}

	public Long countComments() {
		return commentService.count();
	}
}
