package com.graphQLSpring.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Comment {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
    private String body;
    private String createdAt;

    @ManyToOne
    @JoinColumn(name = "author_id",
            nullable = false, updatable = false)
    private Author author;
    
    @ManyToOne
    @JoinColumn(name = "post_id",
            nullable = false, updatable = false)
    private Post post;

    /* Constructor */
	public Comment() {super();}
	public Comment(String body, Author author, Post post, String createdAt) {
		super();
		this.body = body;
		this.author = author;
		this.post = post;
		this.createdAt = createdAt;
	}
	
	/* Getters and Setters */
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	public String getBody() {return body;}
	public void setBody(String body) {this.body = body;}
	
	public String getCreatedAt() {return createdAt;}
	public void setCreatedAt(String createdAt) {this.createdAt = createdAt;}
	
	public Author getAuthor() {return author;}
	public void setAuthor(Author author) {this.author = author;}
	
	public Post getPost() {return post;}
	public void setPost(Post post) {this.post = post;}
	
	/* To String */
	@Override
	public String toString() {
		return "Comment [id=" + id + ", body=" + body + ", createdAt=" + createdAt + ", author=" + author + "]";
	}    
	
	
	
    
    
}
