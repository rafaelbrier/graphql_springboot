package com.graphQLSpring.Model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Post {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
    private String title;
    private String body;
    private String createdAt;
    
    @ManyToOne
    @JoinColumn(name = "author_id",
            nullable = false, updatable = false)
    private Author author;
    
	@OneToMany(mappedBy = "post", targetEntity = Comment.class, cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	private List<Comment> comments;
    
    /* Constructors */
    public Post() {super();}
	public Post(String title, String body, Author author, String createdAt) {
		super();
		this.title = title;
		this.body = body;
		this.author = author;
		this.createdAt = createdAt;
	}
	
	/* Getters and Setters */
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	public String getTitle() {return title;}
	public void setTitle(String title) {this.title = title;}
	
	public String getBody() {return body;}
	public void setBody(String body) {this.body = body;}
	
	public String getCreatedAt() {return createdAt;}
	public void setCreatedAt(String createdAt) {this.createdAt = createdAt;}
	
	public Author getAuthor() {return author;}
	public void setAuthor(Author author) {this.author = author;}
	
	public List<Comment> getComments() {return comments;}
	public void setComments(List<Comment> comments) {this.comments = comments;}
	
	/* To String */
	@Override
	public String toString() {
		return "Post [id=" + id + ", title=" + title + ", body=" + body + ", createdAt=" + createdAt + ", author="
				+ author + "]";
	}
}
